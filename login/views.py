from django.http.request import HttpRequest
from django.http.response import HttpResponse

from django.shortcuts import redirect, render

from django.db import IntegrityError
from django.core.exceptions import ValidationError

from django.contrib import auth, messages
from django.contrib.auth import authenticate,login,logout
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.contrib.sites.shortcuts import get_current_site

from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.utils.encoding import force_text, force_bytes
from django.template.loader import render_to_string


from .tokens import account_activation_token
from .forms import RegistrationForm, LoginForm
# Create your views here.
@login_required
def index(request):
    return render(request,"index.html")

###LOGIN###
def user_login(request):
    data = {}
    template_name = 'login.html'
    if request.user.is_authenticated:
        return redirect('login:index')
    else:
        if request.POST:
            username = request.POST['username']
            password = request.POST['password']
            user = auth.authenticate(
                username=username,
                password=password
            )
            if user is not None:
                if user.is_active:
                    auth.login(request, user)
                    messages.add_message(
                        request,
                        messages.SUCCESS,
                        'Inicio de Sesión Correcto.'
                    )
                    return redirect('login:index')
                else:
                    messages.add_message(
                        request,
                        messages.ERROR,
                        'Cuenta No Activada. Por favor, comprueba la bandeja de entrada de tu correo electrónico.'
                    )
            else:
                messages.add_message(
                    request,
                    messages.SUCCESS,
                    'Datos Incorrectos.'
                )

    return render(request, template_name, data)

@login_required
def user_logout(request):
    logout(request)
    return redirect('login:index')

def activate_account(request, uidb64, token):
    data = {}
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except (TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save()
        messages.add_message(
            request,
            messages.SUCCESS,
            'Cuenta Activada!.'
        )
        return redirect('login:index')
    else:
        messages.add_message(
            request,
            messages.ERROR,
            'Su cuenta no pudo ser activada!.'
        )
        return redirect('login:index')

def registration(request):
    data = {}
    template_name = 'registration.html'
    if request.method == "POST":
        data['form'] = RegistrationForm(request.POST)
        if data['form'].is_valid():
            data['form'].save()
            try:
                user = data['form'].save()
                user.refresh_from_db()
                user.is_active = False
                user.save()
                current_site = get_current_site(request)
                subject = 'StartMATH - Activa tu cuenta.'
                message = render_to_string('activation.html', {
                    'user': user,
                    'domain': current_site.domain,
                    'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                    'token': account_activation_token.make_token(user),
                })
                user.email_user(subject, message)
                messages.add_message(
                    request,
                    messages.SUCCESS,
                    'Activa tu Cuenta.'
                )
                return redirect('login:index')

            except IntegrityError as ie:
                messages.add_message(
                    request,
                    messages.ERROR,
                    'Error mientras se registra usuario: {error}.'.format(error=str(ie))
                )

            except ValidationError as ve:
                messages.add_message(
                    request,
                    messages.ERROR,
                    'Problemas con la validación del formulario: {error}.'.format(error=str(ve))
                )
        else:
            messages.add_message(
                request,
                messages.ERROR,
                'Error mientras se registra usuario.'
            )

    else:
        data['form'] = RegistrationForm()

    return render(request, template_name, data)