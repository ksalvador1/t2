from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User, UserManager
import datetime

class RegistrationForm(UserCreationForm):
    class Meta:
        model = User
        fields=(
            'username',
            'email',
            'first_name', 
            'last_name', 
            'password1', 
            'password2'
            )

"""
Email = forms.EmailField()
First_Name = forms.CharField()
Last_Name = forms.CharField()
Birthday = forms.forms.DateField(initial=datetime.date.today)
"""

class LoginForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)

